package cgi

import java.io.FileWriter
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

/**
  * Created by bing on 5/8/16.
  */
object CountLetter {

  def main(args: Array[String]) {
    val length = 200

    val inputFile = args(0) //load file , need be changed to args[0]
    val out = new FileWriter(args(1))


    //spark config
    val conf = new SparkConf().setAppName("CGI")
    val sc = new SparkContext(conf)

    val time_start = System.currentTimeMillis()
    //create list
    val cg_list = sc.textFile(inputFile).flatMap(_.split("\n"))

    val cg_group_list = cg_list.groupBy(x => x.length)


    val result = cg_group_list.map {
      case (n, list: Seq[String]) => countLetterOnly(n, list)
    }.collect()

    result.foreach {
      case (length, size, data) => {
        out.write(s"\nlen=$length\tnum=$size\n")

        for (i <- 0 until data.length) {
          out.write(s"loci\t$i\n")
          for (j <- 0 until 4;
               k <- 0 until 4)
            out.write(s"${numberToLetter(j)}${numberToLetter(k)}=${data(i)(j)(k)}\t")
          out.write(s"XX=$size\n")
        }


      }
    }
    out.close()

    val time_close = System.currentTimeMillis()
    println(s"time: ${(time_close - time_start) / 1000.0}");

  }
}
