package cgi

import java.io.FileWriter

import scala.io.Source

/**
  * Created by bing on 5/24/16.
  */
object FileProcessor {

  def main(args: Array[String]) {

    val length = 200

    val input = Source.fromFile(args(0))
    val output = new FileWriter(args(1))

    var t = new StringBuilder
    var p = '0'
    var c = '0'
    var s = false

    input.foreach(x => {
      c = letterToNumber(x)
      if (c == '2' && p == '1') {
        if (s) {
          output.write(t.toString)
          output.write('\n')
        } else s = true
        t = new StringBuilder
        t += c
      } else if (c == '4') {
        s = false
      }
      else {
        if (s) {
          t += c
          if (t.length == length) s = false
        }
      }
      p = c
    })

    output.close()

  }

}
