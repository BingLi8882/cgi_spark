import Array._

/**
  * Created by bing on 5/8/16.
  */
package object cgi {

  /**
    * convert letters to numbers
    *
    * @param c "aAtTcCgG"
    * @return "00112233"
    */
  def letterToNumber(c: Char): Char = {
    c match {
      case ('a' | 'A') => '0'
      case ('t' | 'T') => '3'
      case ('c' | 'C') => '1'
      case ('g' | 'G') => '2'
      case ('\n') => '4'
      case _ => '3'
    }
  }

  /**
    * convert numbers to letters
    *
    * @param n 0123
    * @return "ATCG"
    */
  def numberToLetter(n: Int): Char = {
    n match {
      case (0) => 'A'
      case (3) => 'T'
      case (1) => 'C'
      case (2) => 'G'
      case _ => throw new IllegalArgumentException("Wrong Input")
    }
  }

  /**
    * process one group cgi
    *
    * @param length an integer number representing the length of cgi
    * @param list   a seq containing all records
    * @return (Int,Array[Double](length,4),Array[Double](length-1,4,4))
    */
  def countLetter(length: Int, list: Seq[String]) = {
    val total = list.size
    val t_array = ofDim[Int](length, 4)
    val t_letters = ofDim[Int](length - 1, 4, 4)

    list.foreach(x => x.zipWithIndex.foreach {
      case (c, n) => {
        t_array(n)(c.getNumericValue) += 1
        if (n < x.length - 1) {
          t_letters(n)(c.getNumericValue)(x.charAt(n + 1).getNumericValue) += 1
        }
      }
    })

    val result = ofDim[Double](length, 4)

    for (i <- 0 until length;
         j <- 0 until 4)
      result(i)(j) = t_array(i)(j).toDouble / total

    val result1 = ofDim[Double](length - 1, 4, 4)
    for (i <- 0 until length - 1;
         j <- 0 until 4;
         k <- 0 until 4)
      result1(i)(j)(k) = t_letters(i)(j)(k).toDouble / total


    (length ->(result, result1))
  }

  /**
    * process one group cgi
    *
    * @param length an integer number representing the length of cgi
    * @param list   a seq containing all records
    * @return (Int,Array[Double](length-1,4,4))
    */
  def countLetterOnly(length: Int, list: Seq[String]) = {
    val total = list.size
    val result = ofDim[Int](length - 1, 4, 4)
    list.foreach(x => x.zipWithIndex.foreach {
      case (c, n) => if (n < x.length - 1) result(n)(c.getNumericValue)(x.charAt(n + 1).getNumericValue) += 1
    })

    (length, total, result)
  }

  def main(args: Array[String]) {
    val list = Seq("3122", "3132", "3002", "3312")

    val r = countLetter(4, list)

    r._2._1.foreach(x => {
      x.foreach(y => print(y + " "))
      println()
    })

    for (i <- 0 until r._1 - 1) {
      println("at index " + i)
      for (j <- 0 until 4;
           k <- 0 until 4)
        print("" + numberToLetter(j) + numberToLetter(k) + ": " + r._2._2(i)(j)(k) + " ")
      println()
    }
  }

}
