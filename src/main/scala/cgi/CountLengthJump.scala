package cgi

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

/**
  * Created by bing on 5/3/16.
  */
object CountLengthJump {
  def main(args: Array[String]): Unit = {
    val inputFile = "cgi_test" //load file , need be changed to args[0]

    //spark config
    val conf = new SparkConf().setAppName("CGI")
    val sc = new SparkContext(conf)

    //trim if not begin and end with "CG"
    val source_file = sc.textFile(inputFile).map(input => {
      val result = input.toUpperCase()
      result.substring(result.indexOf("CG") + 2, result.lastIndexOf("CG") + 2)
    })

    //create cg list
    val cg_list = source_file.flatMap(input => input.split("CG"))
      .map(s => 'G' + s + 'C')

    //create cg jump list
    val cg_jump_list = cg_list.zipWithIndex().map {
      case (v, i) => i -> v
    }.leftOuterJoin(
      cg_list.zipWithIndex().map {
        case (v, i) => i - 1 -> v
      }
    ).flatMap {
      case (i, (a, b)) => b.map(a -> _)
    }

    //total size
    val size = cg_jump_list.count()

    //count length
    val cg_jump_possibility = cg_jump_list.map {
      case (s1, s2) => s1.length -> s2.length
    }.groupBy(x => x).map {
      case (k, v: Seq[(Int, Int)]) => k -> v.size.toDouble/size
    }



    cg_jump_possibility.saveAsTextFile("out") // output


  }

}
